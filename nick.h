#ifndef NICK_H
#define NICK_H

#include <QColor>

class Nick
{
public:
    Nick();

    void setNickColor(QColor newColor);
    QColor getNickColor();
    QColor pickrandColor();//pick a semi random color initially
    void setNick(QString nick);
    QString getNick();

private:
    QColor nickColor; // the color of a nick
    QString nickname; //the nickname of a a user
};

#endif // NICK_H
