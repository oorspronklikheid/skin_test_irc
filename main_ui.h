#ifndef MAIN_UI_H
#define MAIN_UI_H

#include <QVector>
#include <QtWidgets>
#include <main_chat_text_ui.h>
#include <ticker.h>
#include <nick_menu_ui.h>
#include "theme_settings.h"

class Main_ui : public QWidget
{
    Q_OBJECT

public:
    Main_ui(QWidget *parent = 0 );

private:
    void *createWidget(int type , int parent);
    QLayout *createLayout(int type , int parent=0);

    QWidget *w ;
    Theme_settings *settings ; //Our lifeline to the db

    main_chat_text_ui *test_customui ;
    Main_chat_ticker *chatTicker ;


    QVector<QWidget*> widgets;
    QVector<QLayout*> layouts;

    void create_widgets();

    void setupConnections();

    void setup_refs();

public slots:
    void destroy_widgets() ;

};

#endif // MAIN_UI_H
