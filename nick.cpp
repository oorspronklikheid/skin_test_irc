#include "nick.h"

Nick::Nick()
{
    setNickColor(pickrandColor()); // just chose a nick color for now
}

void Nick::setNickColor(QColor newColor)
{
    nickColor = newColor ;
}

QColor Nick::getNickColor()
{
    return nickColor ;
}

QColor Nick::pickrandColor()
{
    QColor C ;
    C.setRgb(qrand()%128+64,qrand()%128+64,qrand()%128+64);
    return C ;
}

void Nick::setNick(QString nick)
{
    nickname = nick;
}

QString Nick::getNick()
{
    return nickname ;
}
