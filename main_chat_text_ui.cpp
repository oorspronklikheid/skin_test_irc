#include "main_chat_text_ui.h"


main_chat_text_ui::main_chat_text_ui(QWidget *parent) : QWidget(parent)
{
    // Horizontal  main layout
    mainLayout = new QHBoxLayout;

    chat = new QTableWidget(0,3) ; // object that displays the cat state
    mainLayout->addWidget(chat);

    //apply this layout to the widget
    this->setLayout(mainLayout);

}

void main_chat_text_ui::appendChat(QTime timestamp, Nick *nick, QString line)
{
    int row = chat->rowCount() ;
    chat->setRowCount(row + 1);
    QTableWidgetItem *newItem = new QTableWidgetItem(tr("%1").arg(timestamp.toString("HH:mm:ss")));

    chat->setItem(row, 0, newItem);

    QTableWidgetItem *newItem2 = new QTableWidgetItem(nick->getNick());
    newItem2->setTextColor(nick->getNickColor());
    chat->setItem(row, 1, newItem2);

    QTableWidgetItem *newItem3 = new QTableWidgetItem(line);

    chat->setItem(row, 2, newItem3);


    chat->resizeColumnToContents(2);

}

void main_chat_text_ui::appendChat(ChatLine *newLine)
{
    int row = chat->rowCount() ;
    chat->setRowCount(row + 1);
    QTableWidgetItem *newItem = new QTableWidgetItem(tr("%1").arg(newLine->getTimestamp()->toString("HH:mm:ss")));

    chat->setItem(row, 0, newItem);

    QTableWidgetItem *newItem2 = new QTableWidgetItem(newLine->getNickname()->getNick());
    newItem2->setTextColor(newLine->getNickname()->getNickColor());
    chat->setItem(row, 1, newItem2);

    //QTableWidgetItem *newItem3 = new QTableWidgetItem(line);
    QTableWidgetItem *newItem3 = new QTableWidgetItem(newLine->getText());

    chat->setItem(row, 2, newItem3);


    chat->resizeColumnToContents(2);
}

void main_chat_text_ui::appendJoin(QTime timestamp, Nick *nick)
{
    int row = chat->rowCount() ;
    chat->setRowCount(row + 1);
    QTableWidgetItem *newItem = new QTableWidgetItem(tr("%1").arg(timestamp.toString("HH:mm:ss")));

    chat->setItem(row, 0, newItem);

    QTableWidgetItem *newItem2 = new QTableWidgetItem(nick->getNick());
    newItem2->setTextColor(nick->getNickColor());
    chat->setItem(row, 1, newItem2);

    QTableWidgetItem *newItem3 = new QTableWidgetItem(QString("User has enetered chat"));

    chat->setItem(row, 2, newItem3);


    chat->resizeColumnToContents(2);

}

void main_chat_text_ui::appendQuit(QTime timestamp, Nick *nick, QString reason)
{

    int row = chat->rowCount() ;
    chat->setRowCount(row + 1);
    QTableWidgetItem *newItem = new QTableWidgetItem(tr("%1").arg(timestamp.toString("HH:mm:ss")));

    chat->setItem(row, 0, newItem);

    QTableWidgetItem *newItem2 = new QTableWidgetItem(nick->getNick());
    newItem2->setTextColor(nick->getNickColor());
    chat->setItem(row, 1, newItem2);

    QTableWidgetItem *newItem3 = new QTableWidgetItem(QString("User has left chat , reason:") + reason);

    chat->setItem(row, 2, newItem3);


    chat->resizeColumnToContents(2);
}

void main_chat_text_ui::chatJoin(Nick *nick, Channel *channel)
{
    int row = chat->rowCount() ;
    chat->setRowCount(row + 1);

    QTableWidgetItem *newItem = new QTableWidgetItem(tr("%1").arg(QTime::currentTime().toString("HH:mm:ss")));
    chat->setItem(row, 0, newItem);


    QString line;
    line = "User with nickname " + nick->getNick() + " has joined this chatroom" ;

    QTableWidgetItem *newItem3 = new QTableWidgetItem(line);
    chat->setItem(row, 2, newItem3);

    chat->resizeColumnToContents(2);
}

void main_chat_text_ui::chatPart(Nick *nick,Channel *channel)
{


    int row = chat->rowCount() ;
    chat->setRowCount(row + 1);

    QTableWidgetItem *newItem = new QTableWidgetItem(tr("%1").arg(QTime::currentTime().toString("HH:mm:ss")));
    chat->setItem(row, 0, newItem);


    QString line;
    line = "User with nickname " + nick->getNick() + " has left this chatroom" ;
    QTableWidgetItem *newItem3 = new QTableWidgetItem(line);
    chat->setItem(row, 2, newItem3);

    chat->resizeColumnToContents(2);
}

void main_chat_text_ui::setChannel(Channel *newchannel)
{
    channel = newchannel;
    chat->clearContents();
    chat->setRowCount(0);

    QList<QPair<QTime , Nick* > > joinHistory = channel->getJoinHistory() ;
    QList<QPair<QTime , Nick* > > quitHistory = channel->getQuitHistory() ;
    QList<ChatLine *> chatHistory = channel->getHistory() ;
    int i=0 , j=0 , k=0 ;

    while(i < joinHistory.length() ||
          j < quitHistory.length() ||
          k < chatHistory.length())
    {
        qDebug() << "I :" << i <<  " J :" << j << " K :" << k  ;
        qDebug() << joinHistory.length() << quitHistory.length() << chatHistory.length() ;

        if(i < joinHistory.length() && // if alll elements are still in range , then its pretty trivial
           j < quitHistory.length() &&
           k < chatHistory.length())
        {
            qDebug() << "Nothing out of bounds";
            if(joinHistory.at(i).first <= quitHistory.at(j).first &&
               joinHistory.at(i).first <= *chatHistory.at(k)->getTimestamp())
            {
                qDebug() << "I is smallest";
                //append chat with joinHistory.at(i)
                appendJoin(joinHistory.at(i).first , joinHistory.at(i).second);
                i++;
            }else
            if(quitHistory.at(j).first <= joinHistory.at(i).first &&
               quitHistory.at(j).first <= *chatHistory.at(k)->getTimestamp())
            {
                qDebug() << "J is smallest";
                appendQuit(quitHistory.at(j).first , quitHistory.at(j).second , "");
                j++;
            }else
            if(*chatHistory.at(k)->getTimestamp() <= joinHistory.at(i).first &&
               *chatHistory.at(k)->getTimestamp() <= quitHistory.at(j).first )
            {
                qDebug() << "K is smallest";

                appendChat(chatHistory.at(k));
                k++;
            }
        }

        //one of the three indexes is out of bounds
        if(i == joinHistory.length() &&
           j < quitHistory.length() &&
           k < chatHistory.length() )
        {
            qDebug() << "I out of bounds";
            if(quitHistory.at(j).first < *chatHistory.at(k)->getTimestamp())
            {
                appendQuit(quitHistory.at(j).first , quitHistory.at(j).second , "");
                j++;
            }else
            {
                appendChat(chatHistory.at(k));
                k++;
            }
        }
        if(i < joinHistory.length() &&
           j == quitHistory.length() &&
           k < chatHistory.length() )
        {
            qDebug() << "J out of bounds";
            if(*chatHistory.at(k)->getTimestamp() < joinHistory.at(i).first)
            {
                appendChat(chatHistory.at(k));
                k++;
            }else
            {
                appendJoin(joinHistory.at(i).first , joinHistory.at(i).second);
                i++;
            }

        }
        if(i < joinHistory.length() &&
           j < quitHistory.length() &&
           k == chatHistory.length() )
        {
            qDebug() << "K out of bounds";
            if(joinHistory.at(i).first < quitHistory.at(j).first)
            {
                appendJoin(joinHistory.at(i).first , joinHistory.at(i).second);
                i++;

            }else
            {
                appendQuit(quitHistory.at(j).first , quitHistory.at(j).second , "");
                j++;
            }
        }
        //if two of the indexes are out of range
        if(i < joinHistory.length() &&
           j == quitHistory.length() &&
           k == chatHistory.length() )
        {
            qDebug() << "J,K out of bounds";
            i++;
        }
        if(i == joinHistory.length() &&
           j < quitHistory.length() &&
           k == chatHistory.length() )
        {
            qDebug() << "I,K out of bounds";
            appendQuit(quitHistory.at(j).first , quitHistory.at(j).second , "");
            j++;
        }
        if(i == joinHistory.length() &&
           j == quitHistory.length() &&
           k < chatHistory.length() )
        {
            qDebug() << "I,J out of bounds";
            appendChat(chatHistory.at(k));
            k++;
        }

    }
    //find smallest timestamp
//    channel->getJoinHistory();//i
//    channel->getQuitHistory();//j
//    channel->getHistory().length()//k
    //increment


    //add code that updates things...
}
