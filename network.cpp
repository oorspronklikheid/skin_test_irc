#include "network.h"

Network::Network()
{

}

void Network::addChan(Channel *newChan)
{
    channels.append(newChan);
}

void Network::remChan(Channel *delChan)
{
    channels.removeAll(delChan);
}

Channel *Network::at(int i)
{
    return channels.at(i);
}

Channel *Network::at(QString chanName)
{
    for(int i = 0 ; i< channels.length() ;i++)
    {
        if(channels.at(i)->getChanName() == chanName)
        {
            return channels.at(i);
        }
    }
}

int Network::length()
{
    return channels.length();
}

void Network::setNetworkName(QString newName)
{
    networkName = newName ;
}

QString Network::getNetworkName()
{
    return networkName ;
}

void Network::addNick(Nick *newnick, Channel *channel)
{
    channels.at(channels.indexOf(channel))->addNick(newnick) ;
}

void Network::removeNick(Nick *oldnick, Channel *channel)
{
    channels.at(channels.indexOf(channel))->removeNick(oldnick) ;
}
