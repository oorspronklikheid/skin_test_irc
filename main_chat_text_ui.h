#ifndef MAIN_CHAT_TEXT_H
#define MAIN_CHAT_TEXT_H

#include <QtWidgets>
#include <nick.h>
#include <channel.h>

class main_chat_text_ui : public QWidget
{
    Q_OBJECT

public:
    main_chat_text_ui(QWidget *parent = 0 );

private:

      QHBoxLayout *mainLayout ; //main layout for this widget

      QTableWidget *chat ;

      Channel *channel; //current channel

public slots:
    void appendChat(QTime timestamp , Nick *nick , QString line );
    void appendChat(ChatLine *newLine);

    void appendJoin(QTime timestamp , Nick *nick );
    void appendQuit(QTime timestamp , Nick *nick , QString reason );

    void chatJoin(Nick *nick , Channel *channel); // a new user joined , send to listeners

    void chatPart(Nick *nick , Channel *channel); // a current user quits

    void setChannel(Channel *newchannel);
};

#endif // MAIN_CHAT_TEXT_H
