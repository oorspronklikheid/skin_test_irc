#include "theme_settings.h"
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLayout>
#include <qapplication.h>

Theme_settings::Theme_settings()
{
    //init
    dbMap =  QSqlDatabase::addDatabase("QSQLITE");
    dbMap.setDatabaseName("./fileNameOrt.db3");
    if(dbMap.open())
    {
        qDebug() << "db opened";
    }else
    {
        qDebug() << "Failed to open database :1634321864";
    }
    //fin init

     QSqlQuery query(dbMap);
     start();

     //widgets - list of widgets to put in ui
     drop("widgets");
     if(!query.exec("CREATE TABLE widgets ( windex int NOT NULL , widget_type int NOT NULL, widget_parent int NOT NULL , widget_name TEXT , PRIMARY KEY (windex)  ) ; "))
         qDebug() << "fail alert!" << query.lastError() ;
     if(qrand()%1==0)
     {
         if(!query.exec("INSERT INTO widgets VALUES (0,1,0,'pop'),(1,2,1,'pop'),(2,3,0,'pop');"))
             qDebug() << "fail alert!" << query.lastError() ;

     }else
     {
         if(!query.exec("INSERT INTO widgets VALUES (0,1,1,'pop'),(1,2,0,'pop'),(2,3,1,'pop');"))
             qDebug() << "fail alert!" << query.lastError() ;

     }



    //widget_types - list of all the different widgets avail
     drop("widgets_types");
     query.exec("CREATE TABLE widgets_types ( widget_type int NOT NULL , description TEXT NOT NULL , PRIMARY KEY (widget_type) ) ; ");

     if(!query.exec("INSERT INTO widgets_types VALUES (0,'widget'),(1,'Nick_menu_ui'),(2,'main_chat_text_ui') , (3,'channel_tree_ui')"))
        qDebug() << "fail alert!" << query.lastError() ;

    //layouts - list of layouts to be creted
     drop("layouts");
     if(!query.exec("CREATE TABLE layouts ( lindex int NOT NULL , layout_type int NOT NULL , layout_name TEXT , layout_parent int, PRIMARY KEY (lindex)  ) ; "))
         qDebug() << "fail alert!" << query.lastError() ;

     if(!query.exec("INSERT INTO layouts(lindex,layout_type,layout_parent) VALUES (0,0,-1),(1,1,0);"))
        qDebug() << "fail alert!" << query.lastError() ;

    //layout_types  - list of layouts to available
     drop("layout_types");
     if(!query.exec("CREATE TABLE layout_types ( layout_type int NOT NULL , layout_description TEXT , PRIMARY KEY (layout_type)  ) ; "))
         qDebug() << "fail alert!" << query.lastError() ;

     if(!query.exec("INSERT INTO layout_types VALUES (0,'QHBoxLayout'),(1,'QVBoxLayout'),(2,'main_chat_text_ui')"))
         qDebug() << "fail alert!" << query.lastError() ;

    //signals - defines what type of signals are available
     drop("signal");
     if(!query.exec("CREATE TABLE signal( signal_id int NOT NULL , signal_name TEXT , PRIMARY KEY (signal_id)  ) ; "))
         qDebug() << "fail alert!" << query.lastError() ;

     if(!query.exec("INSERT INTO signal VALUES (0,'changeChannel(channel*)')"))
         qDebug() << "fail alert!" << query.lastError() ;

    //slots - defines what type of slots are available
     drop("slots");
     if(!query.exec("CREATE TABLE slots ( slot_id int NOT NULL , slot_name TEXT , PRIMARY KEY (slot_id)  ) ; "))
         qDebug() << "fail alert!" << query.lastError() ;

     if(!query.exec("INSERT INTO slots VALUES (0,'changeChannel(channel*)')"))
         qDebug() << "fail alert!" << query.lastError() ;


    //connections
     drop("connections");
     if(!query.exec("CREATE TABLE connections ( slot_id int NOT NULL , signal_id int NOT NULL , signal_widget int NOT NULL , slot_widget  int NOT NULL , PRIMARY KEY (slot_id)  ) ; "))
         qDebug() << "fail alert!" << query.lastError() ;

     if(!query.exec("INSERT INTO connections VALUES (0,0,2,1)"))
         qDebug() << "fail alert!" << query.lastError() ;

     //table that lists widgets that influence other widgets as a linked list
     drop("metawidgets");
     if(!query.exec("CREATE TABLE metawidgets (controller_id int NOT NULL , view_widget  int NOT NULL , PRIMARY KEY (controller_id , view_widget)  ) ; "))
         qDebug() << "fail alert!" << query.lastError() ;

     if(!query.exec("INSERT INTO metawidgets VALUES (2,1) , (2,0)"))
         qDebug() << "fail alert!" << query.lastError() ;

    getLayouts();
    stop();
    loadqss();

}

QList<QPair<int,int> > Theme_settings::getWidgets()
{
    QList<QPair<int,int> > result;
//    QVector<QLayout*> finresult ;
    QSqlQuery query(dbMap);

    if(!query.exec("SELECT widget_type , widget_parent from widgets ;"))
    {
        qDebug() << "Could not load widgets";
        return result;
    }

    while (query.next())
    {
        QPair<int,int> temp;
        temp.first = query.value(0).toInt();
        temp.second = query.value(1).toInt();

        result.append(temp);

    }
    return result;
}

QList<QList<int> > Theme_settings::getConnections()
{
    QList<QList<int> > result ;
    QSqlQuery query(dbMap);

    if(!query.exec("SELECT slot_id , signal_id , signal_widget , slot_widget  from connections;"))
    {
        qDebug() << "Could not load Connections" << query.lastError() ;
        return result;
    }
    while (query.next())
    {
        QList<int> row;
        row.append(query.value(0).toInt());
        row.append(query.value(1).toInt());
        row.append(query.value(2).toInt());
        row.append(query.value(3).toInt());

        result.append(row);

    }
    return result ;
}

QList<QPair<int, int> > Theme_settings::getReferences()
{
    QList<QPair<int, int> > result ;
    QSqlQuery query(dbMap);

    if(!query.exec("SELECT controller_id , view_widget from metawidgets;"))
    {
        qDebug() << "Could not load Connections" << query.lastError() ;
        return result;
    }
    while (query.next())
    {
        QPair<int, int> row;

        row.first  = query.value(0).toInt();
        row.second = query.value(1).toInt();

        result.append(row);

    }
    return result;
}

void Theme_settings::loadqss()
{
    qApp->setStyleSheet("QLineEdit { background-color: yellow }");
    qApp->setStyleSheet("QTableWidget { background-color: lightyellow }");
}

QList<QPair<int,int> > Theme_settings::getLayouts()
{
    QList<QPair<int,int> > result;
    QSqlQuery query(dbMap);

    if(!query.exec("SELECT layout_type , layout_parent from layouts ;"))
    {
        qDebug() << "Could not load Layouts";
        return result;
    }

    while (query.next())
    {
        QPair<int,int> temp;
        temp.first = query.value(0).toInt();
        temp.second = query.value(1).toInt();

        result.append(temp);

    }

    return result;
}

void Theme_settings::start()
{
    QSqlQuery query(dbMap);
    query.exec("BEGIN");
}

void Theme_settings::drop(QString tblname)
{
    QSqlQuery query(dbMap);

    if(!query.exec(QString("DROP TABLE %1 ;").arg(tblname)))
        qDebug() << "Unable to drop table(" << tblname << ") , Error : "  << query.lastError() ;
}

void Theme_settings::stop()
{
    QSqlQuery query(dbMap);
    query.exec("END");
}
