#include "ticker.h"
#include <QTimer>
#include <QStringList>
#include <nick.h>
#include "qdebug.h"

Main_chat_ticker::Main_chat_ticker()
{
    Channel *channel1 = new Channel ;
    Channel *channel2 = new Channel ;
    Channel *channel3 = new Channel ;

    network1 = new Network ;
    channel1->setChanName("MAINCHATNESS");
    channel2->setChanName("OTHERCHATNESS");
    channel3->setChanName("TESTNESS");
    network1->addChan(channel1);
    network1->addChan(channel2);
    network1->addChan(channel3);

    //connect(this,SIGNAL(chatJoin(Nick*,QString)),channel1,SLOT()


    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(generateRandomLine()));
    timer->start(600);

    nicks << new Nick()<< new Nick()<< new Nick()<< new Nick()<< new Nick() ; //load 5 empty nicknames
    nicks.at(0)->setNick("John");
    nicks.at(1)->setNick("Pword");
    nicks.at(2)->setNick("Lill");
    nicks.at(3)->setNick("IDK");
    nicks.at(4)->setNick("I_keeep_saying_things");


    //Add a few nicks to the first room in network1
    network1->at(0)->addNick(nicks.at(1));
    network1->at(0)->addNick(nicks.at(2));
    network1->at(0)->addNick(nicks.at(3));
    network1->at(0)->addNick(nicks.at(4));

    //Add a few nicks to the Second room in network1
    network1->at(1)->addNick(nicks.at(0));
    network1->at(1)->addNick(nicks.at(2));
    network1->at(1)->addNick(nicks.at(3));



}

Channel *Main_chat_ticker::getChannel()
{
 //return channel1 ; //consider revamping instead of adjusting to network
    qDebug() << "Who can tell me if this is used at all?";
    return network1->at(0);
}

Network *Main_chat_ticker::getNetwork()
{
    return network1 ;
}

void Main_chat_ticker::generateRandomLine()
{
   Nick *nick = new Nick();
  //  QStringList nicks ;
    QStringList chats ;
        chats << "does anyone here use grunt?"
          << "I just find it very stupid that once I install a dependency I have to get the directory through a bunch of folders like node_module/jquery//jquery-version.js when I want to bind it in my html"
          << "is there a better way to do this? this seems more of a hassle than downloading each package on my own"
          << "andralv Using something like Webpack"
          << "never heard of Webpack"
          << "sounds like yet another framework i have to learn, cheesus"
          << "It's not a framework and you don't need to learn every framework :p"
          << "Can you use a domain though? You can use something like dyndns to allow for a domain to work behind a dynamic IP."
          << "I couldn't think of a better channel for this question. Has anyone encountered a situation where Facebook is totally broken and unusable?"
          << "DeltaHeavy honestly I am not sure because I havent been able to try"
          << "I don't know how to make outbound calls that would use my domain"
          << "You can log in, but your news feed is empty, your friends list does not display, most of your privacy settings are locked, and you can only e two most recent\"posts on your timeline."
          << "s/public/publish"
          << "13:46:00 d3m0n The service has a tutorial. It has something called ddclient (though that was over half a decade ago) that updates their nameservers with your\"current IP address. It worked really well for me in the past. You could also buy a $5/mo VPS and domain and install OpenVPN on it to route everything hat. You'd get an IPv4 address and a block of IPv6, all static.";


    nick = ( nicks.at(qrand() % 5) ); //select random mock nick

    if(qrand() % 4 == 0)
    {
        QTime *tmpNow = new QTime(QTime::currentTime());

        ChatLine *tmpLine = new ChatLine(tmpNow, nick, chats.at(qrand()%13));

        if(qrand() % 2 == 0)
            network1->at(0)->addHistoryLine(tmpLine);
        else
            network1->at(1)->addHistoryLine(tmpLine);
    }

    nick = ( nicks.at(qrand() % 5) ); //select random mock nick

    if(qrand() % 10 == 0)
    {
        if(qrand() % 2 == 0)
             network1->at(0)->addNick(nick);
        else
             network1->at(1)->addNick(nick);
    }

    // pretends a random nick quitted wether they were there or not
    if(qrand() % 25 == 0)
    {
        if(qrand() % 2 == 0)
            network1->at(0)->removeNick(nick);
        else
            network1->at(1)->removeNick(nick);
    }

}
