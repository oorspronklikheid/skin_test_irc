#ifndef NETWORK_H
#define NETWORK_H

#include <QObject>
#include <channel.h>

class Network : public QObject
{
    Q_OBJECT
public:
    explicit Network();
    void addChan(Channel *newChan); //adds a new channel to the network
    void remChan(Channel *delChan);//removes a channel from the network

    Channel* at(int i);//returns channel at this index
    Channel* at(QString chanName);
    int length();

    void setNetworkName(QString newName);
    QString getNetworkName(void);

    void addNick(Nick *newnick , Channel *channel); //adds one nick at a time
    void removeNick(Nick *oldnick , Channel *channel); //removes one nick at a time

private:
    QList<Channel*> channels ; // list of all channels assigned to this server
    //QList<pm*> pms; //list all the priv messages TBI
    QString networkName;

signals:

public slots:

};

#endif // NETWORK_H
