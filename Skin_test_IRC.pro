#-------------------------------------------------
#
# Project created by QtCreator 2016-06-22T19:22:19
#
#-------------------------------------------------

QT       += core gui
QT       += widgets
QT       += sql
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Skin_test_IRC

TEMPLATE = app

SOURCES += main.cpp \
    main_ui.cpp \
    main_chat_text_ui.cpp \
    nick_menu_ui.cpp \
    ticker.cpp \
    nick.cpp \
    channel.cpp \
    channel_tree_ui.cpp \
    theme_settings.cpp \
    network.cpp \
    chatline.cpp

HEADERS  += \
    main_ui.h \
    main_chat_text_ui.h \
    nick_menu_ui.h \
    ticker.h \
    nick.h \
    channel.h \
    channel_tree_ui.h \
    theme_settings.h \
    network.h \
    chatline.h

FORMS    +=
