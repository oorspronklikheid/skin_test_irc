#ifndef CHATLINE_H
#define CHATLINE_H

#include <QTime>
#include "nick.h"
// Holds a single chat line
class ChatLine
{
public:
    ChatLine();
    ChatLine(QTime *time,Nick *nick,QString line);


    QTime *getTimestamp() const;
    void setTimestamp(QTime *value);

    Nick *getNickname() const;
    void setNickname(Nick *value);

    QString getText() const;
    void setText(const QString &value);

private:
    QTime *timestamp;
    Nick *nickname;
    QString text;

};

#endif // CHATLINE_H
