#include "main_ui.h"
#include <QApplication>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QTimer>
#include "channel_tree_ui.h"

//#include <main_chat_text_ui.h>

Main_ui::Main_ui(QWidget *parent) : QWidget(parent)
{
    qsrand(QTime::currentTime().msec() );

    settings = new Theme_settings();
    chatTicker = new Main_chat_ticker ;

    create_widgets();
    setup_refs();

///    hLayout->addWidget(test_customui,80); //in auto adding , add those ints that determine stretch
///    hLayout->addWidget(nickBar,20);

}

void *Main_ui::createWidget(int type, int parent)
{
    if(type==0)
    {

        w = new QWidget();
        w->setLayout(layouts.at(parent));
        w->show();
    }
    if(type==1)
    {
        //create widget
        Nick_menu_ui *newwidget = new Nick_menu_ui(w);

        widgets.append(newwidget);


        layouts.at(parent)->addWidget(newwidget);
        newwidget->setSizePolicy(QSizePolicy::Fixed , QSizePolicy::Preferred);

        //connect our new widget up to things
        Channel *channel1= chatTicker->getChannel();
        //qDebug() <<"widget1 created" ;
        newwidget->setChannel(channel1);

        QObject::connect(channel1,SIGNAL(chatJoin(Nick*,Channel*)),newwidget,SLOT(chatJoin(Nick*,Channel*)));
        QObject::connect(channel1,SIGNAL(chatPart(Nick*,Channel*)),newwidget,SLOT(chatPart(Nick*,Channel*)));

      }
    if(type==2)
    {
//     qDebug() <<"widget2 created" ;
     //create widget
     main_chat_text_ui *newchat = new main_chat_text_ui(w);

     widgets.append(newchat);
     layouts.at(parent)->addWidget(newchat);

     //connect our new widget up to things
     Channel *channel1= chatTicker->getChannel();

     QObject::connect(channel1,SIGNAL(chatJoin(Nick*,Channel*)),newchat,SLOT(chatJoin(Nick*,Channel*)));
     QObject::connect(channel1,SIGNAL(chatPart(Nick*,Channel*)),newchat,SLOT(chatPart(Nick*,Channel*)));
//     QObject::connect(chatTicker,SIGNAL(appendChat(QTime,Nick*,QString)),newchat,SLOT(appendChat(QTime,Nick*,QString)));//deprecated
     QObject::connect(channel1,SIGNAL(appendChat(ChatLine*)),newchat,SLOT(appendChat(ChatLine*)));


    }
    if(type==3)
    {
//        qDebug() <<"widget3 created" ;
        //create widget
        channel_tree_ui *newwidget = new channel_tree_ui(w);

        widgets.append(newwidget);
        layouts.at(parent)->addWidget(newwidget);

        Network *network = chatTicker->getNetwork();

        newwidget->setNetwork(network);
        /*
        //connect our new widget up to things
        Channel *channel1= chatTicker->getChannel();

        QObject::connect(channel1,SIGNAL(chatJoin(Nick*,Channel*)),newchat,SLOT(chatJoin(Nick*,Channel*)));
        QObject::connect(channel1,SIGNAL(chatPart(Nick*,Channel*)),newchat,SLOT(chatPart(Nick*,Channel*)));
        QObject::connect(chatTicker,SIGNAL(appendChat(QTime,Nick*,QString)),newchat,SLOT(appendChat(QTime,Nick*,QString)));

        */
    }
}

QLayout *Main_ui::createLayout(int type, int parent)
{
    QLayout *newlayout ;

    if(type == 0)
    {
        newlayout = new QHBoxLayout ;
    }
    if(type == 1)
    {
        newlayout = new QVBoxLayout ;
    }
    if(type == 2)
    {
        newlayout = new QGridLayout ;
    }
    if(parent!=-1)
    {
        if(qobject_cast<QHBoxLayout*>(layouts.at(parent))!=Q_NULLPTR)
        {
            (qobject_cast<QHBoxLayout*>(layouts.at(parent)))->addLayout(newlayout);
        }
        if(qobject_cast<QVBoxLayout*>(layouts.at(parent))!=Q_NULLPTR)
        {
            (qobject_cast<QVBoxLayout*>(layouts.at(parent)))->addLayout(newlayout);
        }
        if(qobject_cast<QGridLayout*>(layouts.at(parent))!=Q_NULLPTR)
        {
            qDebug() << "cant use this class as a parent , yet";
        }
    }
    layouts.append(newlayout); //append to layouts container
    return newlayout ;
}

void Main_ui::create_widgets()
{
    layouts.clear();

    QList<QPair<int,int> > templayouts = settings->getLayouts();
    for(int i = 0 ; i < templayouts.length() ; i++)
    {
        createLayout(templayouts.at(i).first, templayouts.at(i).second);
    }
    createWidget(0, 0); //Initiates parent widget W before creating other widgets

    QList<QPair<int,int> > tempwidgets= settings->getWidgets();
    for(int i = 0 ; i < tempwidgets.length() ; i++)
    {
        createWidget(tempwidgets.at(i).first,tempwidgets.at(i).second);
    }
}

void Main_ui::setupConnections()
{
    qDebug() << "Depricated , use setup_refs instead";
//    QList<QList<int> > temp = settings->getConnections();
//    qDebug() << temp;


//    for(int i = 0 ; i < temp.length() ; i++)
//    {
//        QWidget* tempSignalW = widgets.at(temp.at(i).at(2));
//        QWidget* tempSlotW = widgets.at(temp.at(i).at(3));
//        qDebug() << "tempSignalW" << tempSignalW ;
//        qDebug() << "tempSlotW" << tempSlotW ;
//        if(temp.at(i).at(0) == 0 && temp.at(i).at(1) == 0)
//        {
//            qDebug() << "setting connection" ;
//           // QObject::connect(tempSignalW,SIGNAL(changeChannel(Channel*)),tempSlotW,SLOT(setChannel(Channel *)));
//            //
//        }

//    }
}

void Main_ui::setup_refs()
{
  QList<QPair<int, int> > temp = settings->getReferences();;

  for(int i = 0 ; i < temp.length() ; i++)
  {
      QWidget* metaWidget = widgets.at(temp.at(i).first);
      QWidget* viewWidget = widgets.at(temp.at(i).second);

      if(qobject_cast<channel_tree_ui*>(metaWidget) != 0) //we found a channel_tree_ui widget
      {
          channel_tree_ui* tmpChlTreeUi = qobject_cast<channel_tree_ui*>(metaWidget) ;
          tmpChlTreeUi->addView(viewWidget);
      }
  }
}

void Main_ui::destroy_widgets()
{
    for(int i = 0 ; i < widgets.length() ; i++)
    {
        QWidget *widget = widgets.at(i); //grab a widget
        widgets.remove(i); //remove from list
//        widget->~QWidget() ;//Let there be death
        delete widget;


    }
    w->hide();//hide the main bitch
    qDebug() <<"widgets destroyed" ;
    create_widgets();
}

