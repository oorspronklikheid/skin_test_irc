#ifndef CHANNELCONTAINER_H
#define CHANNELCONTAINER_H

#include <QString>
#include <nick.h>
#include <QObject>
#include "chatline.h"

/*
 *Holds data about a specific channel
 *
 */

class Channel: public QObject
{
    Q_OBJECT

public:
    Channel();

    QString getChanName() const;
    void setChanName(const QString &value);

    QString getChanTopic() const;
    void setChanTopic(const QString &value);

    QList<Nick *> getNicks() const; // gets all the nicks
    void setNicks(const QList<Nick *> &value); //sets all the nicks

    QList<ChatLine *> getHistory() const;
    void setHistory(const QList<ChatLine *> &value);

    void addNick(Nick *newnick); //adds one nick at a time
    void removeNick(Nick *oldnick); //adds one nick at a time
    void addHistoryLine(ChatLine *line);

    QList<QPair<QTime, Nick *> > getJoinHistory() const;
    void setJoinHistory(const QList<QPair<QTime, Nick *> > &value);

    QList<QPair<QTime, Nick *> > getQuitHistory() const;
    void setQuitHistory(const QList<QPair<QTime, Nick *> > &value);

private:
    QString chanName;
    QString chanTopic;

    QList< Nick* > nicks; // all the nick names in a channel
    QList<ChatLine*> history; //stores the chat history

    QList<QPair<QTime , Nick* > >  joinHistory ; //stores the join history
    QList<QPair<QTime , Nick* > >  quitHistory ; //stores the quit history


//public slots:
//    void addUser(Nick *nick , Channel *channel); // a new user joined , send to listeners
//    void remUser(Nick *nick , Channel *channel); // a current user quits

signals:
    void chatJoin(Nick *nick , Channel *channel); // a new user joined , send to listeners
    void chatPart(Nick *nick , Channel *channel); // a current user quits

    void appendChat(ChatLine* line); //something was said
};

#endif // CHANNELCONTAINER_H
