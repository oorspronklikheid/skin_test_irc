#include "nick_menu_ui.h"

Nick_menu_ui::Nick_menu_ui(QWidget *parent): QWidget(parent)
{

    mainLayout = new QHBoxLayout;

    nickbar = new QTableWidget(0,1) ;
    mainLayout->addWidget(nickbar);

    //appendChat(QTime(QTime::currentTime()) , QString("igreetyou") , QString("How are you"));


    //aply this layout to the widget
    this->setLayout(mainLayout);

}

void Nick_menu_ui::bootstrap()
{

}

void Nick_menu_ui::chatJoin(Nick *nick , Channel *channel)
{

    int row = nickbar->rowCount() ;
    nickbar->setRowCount(row + 1);

    QTableWidgetItem *newItem = new QTableWidgetItem(nick->getNick());
    newItem->setTextColor(nick->getNickColor());
    nickbar->setItem(row, 0, newItem);

    nickbar->resizeColumnToContents(0);
}

void Nick_menu_ui::chatPart(Nick *nick, Channel *channel)
{

    for(int i = 0 ; i < nickbar->rowCount() ; i++)
    {
        if(nickbar->item(i,0)->text() == nick->getNick())
        {
            nickbar->removeRow(i);
            //if(i>-1)//we removed an item , adjust our index
                i--;
        }
    }
}
/*
 * When we switch channels
 *
 *
 */
void Nick_menu_ui::setChannel(Channel *newchannel)
{
    channel = newchannel;
    nickbar->clearContents();
    nickbar->setRowCount(0);


    for(int i = 0 ; i < channel->getNicks().length() ; i++ ) //foreach nick in the channel
    {
        int row = nickbar->rowCount() ;
        nickbar->setRowCount(row + 1);

        QTableWidgetItem *newItem = new QTableWidgetItem(channel->getNicks().at(i)->getNick());
        newItem->setTextColor(channel->getNicks().at(i)->getNickColor());
        nickbar->setItem(row, 0, newItem);
        nickbar->resizeColumnToContents(0);

    }


}





