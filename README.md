# README #

This project is part of a test I'm doing to see how one would go about coding a highly flexible UI. 

By Highly flexible i mean three main concepts. 
1. The styling of each and every component should be adjustable post compilation ( this is largely achieved by QSS files). 
2. The layout should also be adjustable via some user interaction. 
3. There should be redundant UI widgets that displays different combinations of the same data and meta data which allows for more flexibility in layout.

I'll measure success if the only reliable way to describe a theme is various screen shots or installing the theme itself. Second level of success is if i can fool people into thinking its some other IRC client actually. 
