#ifndef CHANNEL_TREE_UI_H
#define CHANNEL_TREE_UI_H

#include <QtWidgets>
#include <network.h>

class channel_tree_ui : public QWidget
{
    Q_OBJECT

public:
    channel_tree_ui(QWidget *parent = 0 );

    void setNetwork(Network *newnetwork);

    void addView(QWidget* newView);
private:
    QLayout *mainLayout;

    QTreeView *channels_view;

    Network *currentnewtwork;

    QList <QWidget*> widgets;

public slots:
    void channelclicked(QTreeWidgetItem *item,int column);
    /*revise , look at channel
    void addServer();
    void RemServer();
    void addChan();
    void RemChan();
    //*///revise
signals:
    void changeChannel(Channel *selectedchannel);
};

#endif // CHANNEL_TREE_UI_H
