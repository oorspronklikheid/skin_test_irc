#ifndef MAIN_CHAT_TICKER_H
#define MAIN_CHAT_TICKER_H

#include <QObject>
#include <QTime>
#include <nick.h>
#include <channel.h>
#include "network.h"


/*
 * This class generates fake data as if we are connected to a real
 * IRC server and we have already parsed the raw data. Later we may
 * drop in a reall class with live data but for the current goal this is fine
 */
class Main_chat_ticker: public QObject
{
    Q_OBJECT

public:
    Main_chat_ticker();
    Channel *getChannel(); //returns current channel , not sure if this is the correct way to do this , but I need to do something
    Network *getNetwork();
signals:

public slots:
    void generateRandomLine(); // pretends a user said something , triggered by a timer internally

private:
    QList< Nick* > nicks;
    //Channel *channel1 ; //DEPRICATED ** for now we have a single room ^^
    Network *network1 ; //for now we have one network :O


};

#endif // MAIN_CHAT_TICKER_H
