#include "channel.h"
#include <QDebug>

Channel::Channel()
{
}
QString Channel::getChanName() const
{
    return chanName;
}

void Channel::setChanName(const QString &value)
{
    chanName = value;
}

QString Channel::getChanTopic() const
{
    return chanTopic;
}

void Channel::setChanTopic(const QString &value)
{
    chanTopic = value;
}

QList<Nick *> Channel::getNicks() const
{
    return nicks;
}

void Channel::setNicks(const QList<Nick *> &value)
{
    nicks = value;
}

void Channel::addNick(Nick *newnick)
{
    nicks.append(newnick);
    emit chatJoin(newnick , this); // a new user joined , send to listeners
//    joinHistory.append(QPair(QTime(QTime::currentTime()),newnick));
    QPair<QTime , Nick*  > temp ;
    temp.first = QTime(QTime::currentTime()) ;
    temp.second = newnick;
    joinHistory.append(temp);

}

void Channel::removeNick(Nick *oldnick)
{
    nicks.removeAll(oldnick) ;
    emit chatPart(oldnick , this); // a current user quits

    QTime *tmpNow = new QTime(QTime::currentTime());

//    history.append(new ChatLine(tmpNow,new Nick,QString("User has parted")));
    QPair<QTime , Nick*  > temp ;
    temp.first = QTime(QTime::currentTime()) ;
    temp.second = oldnick ;
    quitHistory.append(temp);
}

void Channel::addHistoryLine(ChatLine *line)
{
    history.append(line);
    emit appendChat(line);
}

QList<QPair<QTime, Nick *> > Channel::getJoinHistory() const
{
    return joinHistory;
}

void Channel::setJoinHistory(const QList<QPair<QTime, Nick *> > &value)
{
    joinHistory = value;
}

QList<QPair<QTime, Nick *> > Channel::getQuitHistory() const
{
    return quitHistory;
}

void Channel::setQuitHistory(const QList<QPair<QTime, Nick *> > &value)
{
    quitHistory = value;
}

QList<ChatLine *> Channel::getHistory() const
{
    return history;
}

void Channel::setHistory(const QList<ChatLine *> &value)
{
    history = value;
}

