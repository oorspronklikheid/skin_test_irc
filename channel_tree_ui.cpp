#include "channel_tree_ui.h"
#include "main_chat_text_ui.h"
#include "nick_menu_ui.h"

channel_tree_ui::channel_tree_ui(QWidget *parent) : QWidget(parent)
{
    // Horizontal  main layout
    mainLayout = new QHBoxLayout;
/*
    channels_view = new QTreeView(); // object that displays the cat state

    QFileSystemModel *model = new QFileSystemModel;
    model->setRootPath(QDir::currentPath());
    //= new QTreeView();
    channels_view->setModel(model);
    */


    /*treeviewtest

    QTreeWidget *treeWidget = new QTreeWidget();
    treeWidget->setColumnCount(1);
    QList<QTreeWidgetItem *> items;
    for (int i = 0; i < 10; ++i)
        items.append(new QTreeWidgetItem((QTreeWidget*)0, QStringList(QString("item: %1").arg(i))));
    items.append(new QTreeWidgetItem(items.at(5), QStringList(QString("item: %1").arg(15))));
    treeWidget->insertTopLevelItems(0, items);
    //*///treeviewtest


//    mainLayout->addWidget(channels_view);

    //QTreeModel PENGUIN;
    //apply this layout to the widget
    this->setLayout(mainLayout);
}

void channel_tree_ui::setNetwork(Network *newnetwork)
{
    currentnewtwork  = newnetwork;

    QTreeWidget *treeWidget = new QTreeWidget();
    treeWidget->setColumnCount(1);
    QList<QTreeWidgetItem *> items;
    for(int i = 0 ; i< newnetwork->length() ; i++)
    {
        QTreeWidgetItem *tempQTreeWidgetItem = new QTreeWidgetItem((QTreeWidget*)0, QStringList(newnetwork->at(i)->getChanName()));

        items.append(tempQTreeWidgetItem);
    }
    connect(treeWidget,SIGNAL(itemClicked(QTreeWidgetItem*,int)),this,SLOT(channelclicked(QTreeWidgetItem*,int)));
    mainLayout->addWidget(treeWidget);
    treeWidget->insertTopLevelItems(0, items);

}

void channel_tree_ui::addView(QWidget *newView)
{
  widgets.append(newView);

  if(qobject_cast<channel_tree_ui *>(newView) != 0) //we found a channel_tree_ui widget
  {
    channel_tree_ui *temp = qobject_cast<channel_tree_ui *>(newView) ;

  }
  if(qobject_cast<Nick_menu_ui *>(newView) != 0) //we found a Nick_menu_ui
  {
    Nick_menu_ui *temp = qobject_cast<Nick_menu_ui *>(newView) ;
    connect(this,SIGNAL(changeChannel(Channel*)),temp,SLOT(setChannel(Channel*)));
  }
  if(qobject_cast<main_chat_text_ui *>(newView) != 0) //we found a main_chat_text_ui
  {
    main_chat_text_ui *temp = qobject_cast<main_chat_text_ui *>(newView) ;
    connect(this,SIGNAL(changeChannel(Channel*)),temp,SLOT(setChannel(Channel*)));
  }
}

void channel_tree_ui::channelclicked(QTreeWidgetItem *item, int column)
{
    for(int i = 0 ; i< currentnewtwork->length() ; i++)
    {
        if(currentnewtwork->at(i)->getChanName() == item->text(column))
        {
            qDebug() << "Emitting channel click!";
            emit changeChannel(currentnewtwork->at(i));
        }
    }
    //resolve channel

    //emit some sort of signal here
}






