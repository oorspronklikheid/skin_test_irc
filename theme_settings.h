#ifndef THEME_SETTINGS_H
#define THEME_SETTINGS_H

#include <QObject>
#include <QtSql/QtSql>
#include <QVariant>

class Theme_settings : public QObject
{
       Q_OBJECT
public:
    Theme_settings();
    QList<QPair<int, int> > getLayouts();
    QList<QPair<int, int> > getWidgets();
    QList<QList<int> > getConnections();
    QList<QPair<int, int> >getReferences();

    void loadqss(void);//Loads qss from db in order to apply the stored style of the widgets -- well it will do that some day

private:

    QSqlDatabase dbMap ;

    void stop();
    void start();
    void drop(QString tblname); //drop a table
};

#endif // THEME_SETTINGS_H
