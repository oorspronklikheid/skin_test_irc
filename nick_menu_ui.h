#ifndef NICK_MENU_UI_H
#define NICK_MENU_UI_H

#include <QtWidgets>
#include <nick.h>
#include <channel.h>

class Nick_menu_ui: public QWidget
{
    Q_OBJECT
public:
    Nick_menu_ui(QWidget *parent = 0 );
    /*
     *when this widget is created this needs
     *to be called so we can set stuff. At a minimum we need to list all the nicknames
     */
    void bootstrap();

private:
    QHBoxLayout *mainLayout ; //main layout for this widget

    QTableWidget *nickbar ;

    Channel *channel; //current channel

public slots:
    void chatJoin(Nick *nick , Channel *channel);

    void chatPart(Nick *nick , Channel *channel);

    void setChannel(Channel *newchannel);

    //void updateNick(); //todo implement

};

#endif // NICK_MENU_UI_H
