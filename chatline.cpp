#include "chatline.h"

ChatLine::ChatLine()
{

}

ChatLine::ChatLine(QTime *time, Nick *nick, QString line)
{
   timestamp = time;
   nickname = nick;
   text = line;

}
QTime *ChatLine::getTimestamp() const
{
    return timestamp;
}

void ChatLine::setTimestamp(QTime *value)
{
    timestamp = value;
}
Nick *ChatLine::getNickname() const
{
    return nickname;
}

void ChatLine::setNickname(Nick *value)
{
    nickname = value;
}
QString ChatLine::getText() const
{
    return text;
}

void ChatLine::setText(const QString &value)
{
    text = value;
}


